# Chung cư AQH Riverside Thượng Thanh
[Chung cư AQH Riverside Thượng Thanh](http://northernhomes.vn/chung-cu-aqh-riverside-thuong-thanh-khoi-nguon-hanh-phuc.html) tọa lạc tại tổ 18 phường Thượng Thanh, quận Long Biên, Hà Nội. Tiếp giáp ngay mặt đường Gia Thượng thông thoáng cùng tầm view Sông Đuống chọn vẹn. Chung cư AQH Riverside Thượng Thanh được thiết kế với những căn hộ diện tích nhỏ từ 56m2 đến 72m2 nhưng vẫn đảm bảo các loại căn hộ từ 2 phòng ngủ đến 3 phòng ngủ hướng đến phân khúc tầm trung nhưng chất lượng cũng như tiện ích cao cấp hứa hẹn sẽ là chốn an cư lý tưởng với cư dân tương lai.
🔸 Tên thương mại dự án:[ AQH Riverside](http://northernhomes.vn/chung-cu-aqh-riverside-thuong-thanh-khoi-nguon-hanh-phuc.html)
🔸 Vị trí dự án: Tọa lạc tại tổ 18 phường Thượng Thanh, quận Long Biên, TP. Hà Nội.
🔸 Chủ đầu tư: Công Ty TNHH An Quý Hưng
🔸 Tư vấn thiết kế: Công ty cổ phần tư vấn đầu tư xây dựng phát triển đô thị Hà Nội và Công ty cổ phần giải pháp và công nghệ xây dựng SF
🔸 Tư vấn giám sát: Công ty cổ phần tư vấn công nghệ, thiệt bị và kiểm định xây dựng CONINCO
🔸 Diện tích đất: ~7.000m2
🔸 Quy mô dự án:  Gồm 1 tòa chung cư và 23 căn thấp tầng.
🔸 Bàn giao: đang cập nhật.

Bản quyền bài viết thuộc về: [http://northernhomes.vn/chung-cu-aqh-riverside-thuong-thanh-khoi-nguon-hanh-phuc.html](http://northernhomes.vn/chung-cu-aqh-riverside-thuong-thanh-khoi-nguon-hanh-phuc.html)